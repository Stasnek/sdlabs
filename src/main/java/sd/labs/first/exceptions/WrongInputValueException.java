package sd.labs.first.exceptions;

/**
 * Created by makst on 16.04.2017.
 */
public class WrongInputValueException extends Exception {

    public WrongInputValueException(String message) {
        super(message);
    }
}
