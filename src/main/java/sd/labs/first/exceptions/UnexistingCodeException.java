package sd.labs.first.exceptions;

/**
 * Created by makst on 15.04.2017.
 */
public class UnexistingCodeException extends Exception {

    public UnexistingCodeException(String message) {
        super(message);
    }
}
