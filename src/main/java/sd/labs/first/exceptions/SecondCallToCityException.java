package sd.labs.first.exceptions;

/**
 * Created by makst on 16.04.2017.
 */
public class SecondCallToCityException extends Exception {

    public SecondCallToCityException(String message) {
        super(message);
    }
}
