package sd.labs.first;

import dnl.utils.text.table.TextTable;
import sd.labs.first.exceptions.SecondCallToCityException;
import sd.labs.first.exceptions.UnexistingCodeException;
import sd.labs.first.exceptions.WrongInputValueException;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

import static sd.labs.first.Constants.*;

/**
 * Created by makst on 15.04.2017.
 * M=8 N=4
 * <p>
 * № направления	1	7	12	14
 * время (мин.) 	6	5	3	4
 * <p>
 * № направления	Город	Код города	Тариф (руб./мин.)
 * 1	    Астрахань	    851	        8,80
 * 7	        Омск	    381	        11,50
 * 12	      Хабаровск	    421	        13,30
 * 14	        Рига	    370	        15,80
 */

public class BillingSystem {
    private Set<CallInstance> callInstances = new HashSet<>();
    private Set<CityTariff> cityTariffs = new HashSet<>();
    private Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        new BillingSystem().run();
    }

    private void printReport() {
        int calls = callInstances.size();
        double totalPrice = 0;
        int totalMinutes = 0;
        double minTariffPrice = calls > 0 ? Double.MAX_VALUE : 0;
        int specialCallsCount = 0; // >35rub price
        NumberFormat formatter = new DecimalFormat("#0.00");
        Object[][] data = new Object[calls][5];
        int i = 0;
        for (CallInstance call : callInstances) {
            CityTariff tariff = call.getCityTariff();
            totalPrice += call.getPrice();
            totalMinutes += call.getMinutes();
            if (call.getPrice() > 35) {
                specialCallsCount++;
            }
            if (tariff.getTariffPrice() < minTariffPrice) {
                minTariffPrice = tariff.getTariffPrice();
            }
            data[i][0] = tariff.getCode();
            data[i][1] = call.getMinutes();
            data[i][2] = tariff.getTariffPrice();
            data[i][3] = formatter.format(call.getPrice());
            data[i][4] = tariff.getCityName();
            i++;
        }
        printTable(data);
        System.out.println();
        System.out.println("Стоимость: " + formatter.format(totalPrice));
        System.out.println("Минимальный тариф: " + formatter.format(minTariffPrice));
        System.out.println("Всего минут в разговоре: " + totalMinutes);
        System.out.println("Звонков дороже 35 руб: " + specialCallsCount);
    }

    private void printTable(Object[][] data) {
        String[] columnNames = {
                "Код города",
                "Время, мин",
                "Тариф, руб",
                "Стоимость, руб",
                "Город"};

        TextTable tt = new TextTable(columnNames, data);
        tt.setAddRowNumbering(true);
        tt.printTable();
    }

    private void createData() {
        cityTariffs.add(new CityTariff(RIGA, RIGA_CODE, 15.80));
        cityTariffs.add(new CityTariff(KHABAROVSK, KHABAROVSK_CODE, 13.30));
        cityTariffs.add(new CityTariff(OMSK, OMSK_CODE, 11.50));
        cityTariffs.add(new CityTariff(ASTRAKHAN, ASTRAKHAN_CODE, 8.80));
    }

    private double getMinimumTariffPrice() {
        double min = Double.MAX_VALUE;
        for (CallInstance call : callInstances) {
            if (call.getCityTariff().getTariffPrice() < min) {
                min = call.getCityTariff().getTariffPrice();
            }
        }
        return min;
    }

    private CityTariff getTariffByCode(int code) throws UnexistingCodeException {
        for (CityTariff tariff : cityTariffs) {
            if (tariff.getCode() == code) {
                return tariff;
            }
        }
        throw new UnexistingCodeException("Unexisting code: " + code);
    }

    private void printCodes() {
        for (CityTariff tariff : cityTariffs) {
            System.out.println(tariff.getCode() + " - " + tariff.getCityName());
        }
    }

    private int checkForSecondCallToCity(String value) throws WrongInputValueException, SecondCallToCityException {
        int code = checkValue(value);
        for (CallInstance call : callInstances) {
            if (call.getCityTariff().getCode() == code) {
                throw new SecondCallToCityException("You have already talked to "
                        + call.getCityTariff().getCityName());
            }
        }
        return code;
    }

    private int checkValue(String value) throws WrongInputValueException {
        int intValue = Integer.parseInt(value);
        if (intValue <= 0) {
            throw new WrongInputValueException("Value must be > 0");
        }
        return intValue;
    }

    private void makeCall() {
        try {
            System.out.println("Input code:");
            int code = checkForSecondCallToCity(scanner.next());
            CityTariff cityTariff = getTariffByCode(code);
            System.out.println("City: " + cityTariff.getCityName());
            System.out.println("Tariff: " + cityTariff.getTariffPrice());
            System.out.println("Input minutes:");
            scanner = new Scanner(System.in);
            int minutes = checkValue(scanner.next());
            callInstances.add(new CallInstance(cityTariff, minutes));
        } catch (UnexistingCodeException | WrongInputValueException | SecondCallToCityException e) {
            System.out.println(e.getMessage());
        } catch (NumberFormatException e) {
            System.out.println("Wrong value format!");
        }
    }

    private void run() {
        createData();
        printCodes();
        while (true) {
            makeCall();
            if (makeAnotherCall()) {
                System.out.println(System.lineSeparator() + "Creating report...");
                break;
            }
            System.out.println();
        }
        printReport();
    }

    private boolean makeAnotherCall() {
        System.out.println("Make another call? (y/n)");
        return !scanner.next().equals("y");
    }
}
