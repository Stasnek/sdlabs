package sd.labs.first;

/**
 * Created by makst on 15.04.2017.
 */
public class CallInstance {
    private CityTariff cityTariff;
    private int minutes;

    public CallInstance(CityTariff cityTariff, int minutes) {
        this.cityTariff = cityTariff;
        this.minutes = minutes;
    }

    public CallInstance() {

    }

    public CityTariff getCityTariff() {
        return cityTariff;
    }

    public void setCityTariff(CityTariff cityTariff) {
        this.cityTariff = cityTariff;
    }


    public Double getPrice() {
        return minutes * cityTariff.getTariffPrice();
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }
}
