package sd.labs.first;

/**
 * Created by makst on 15.04.2017.
 */
public final class Constants {
    public static final String ASTRAKHAN = "Астрахань";
    public static final String OMSK = "Омск";
    public static final String KHABAROVSK = "Хабаровск";
    public static final String RIGA = "Рига";
    public static final int ASTRAKHAN_CODE = 851;
    public static final int OMSK_CODE = 381;
    public static final int KHABAROVSK_CODE = 421;
    public static final int RIGA_CODE = 370;

    private Constants() {

    }
}
