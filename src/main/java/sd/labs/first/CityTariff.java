package sd.labs.first;

/**
 * Created by makst on 15.04.2017.
 */
public class CityTariff {
    private String cityName;
    private int code;
    private Double tariffPrice;


    public CityTariff(String cityName, int code,Double tariffPrice) {
        this.cityName = cityName;
        this.code = code;
        this.tariffPrice = tariffPrice;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Double getTariffPrice() {
        return tariffPrice;
    }

    public void setTariffPrice(Double tariffPrice) {
        this.tariffPrice = tariffPrice;
    }
}
